package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class DeleteTweet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String tite = request.getParameter( "title" );
        String desc = request.getParameter( "description" );

        try {

            int result = new TweetRepository().DeleteTweet( tite, desc );
            if (result > 0) {
                out.println("You are successfully deleted tweet: " + tite );
                out.println();
                RequestDispatcher rs = request.getRequestDispatcher( "index3.jsp" );
                rs.include( request, response );
            }

        } catch (Exception se) {
            se.printStackTrace();
            response.setStatus( 500 );
            out.println( se.getMessage() );
        }
    }
}
