package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ListTweet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String tit = request.getParameter( "title" );
        String desc = request.getParameter( "description" );
        String auth = request.getParameter( "author" );
        String date = request.getParameter( "createdDate" );

        try {
            List<TweetModel> allTweets = new TweetRepository().getAllTweets();

            for (TweetModel t: allTweets) {
                out.print( " date of creation: " + t.getCreatedDate() );
                out.print( " author: " + t.getAuthor() );
                out.print( " title: " + t.getTitle() );
                out.print( " description: " + t.getDescription() + "<br>" );
            }

        } catch (Exception se) {
            se.printStackTrace();
            response.setStatus( 500 );
            System.out.println( se.getMessage() );
        }
    }
}