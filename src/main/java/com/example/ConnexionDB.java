package com.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionDB {

    private static Connection connection = null;

    public static Connection ConnexionBase() throws SQLException {

        String DbUrl = "jdbc:mysql://localhost:3306/testforjava?serverTimezone=UTC";
        String DbDriver = "com.mysql.jdbc.Driver";
        String DbUser = "root";
        String DbPwd = "1234";

        try {
            Class.forName( DbDriver );
            connection = DriverManager.getConnection
                    ( DbUrl, DbUser, DbPwd );
            return connection;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}
