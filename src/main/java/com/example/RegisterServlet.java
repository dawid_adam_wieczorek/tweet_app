package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class RegisterServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

        String name = request.getParameter("name");
        String email = request.getParameter("email");

        try {

            if (Validate.checkExistentAccount( name, email)) {
                RequestDispatcher rs = request.getRequestDispatcher( "AccountAdd");
                rs.forward( request, response );
            } else {
                out.println( "The account with email: \"" + email + "\" already exist." );
                RequestDispatcher rs = request.getRequestDispatcher( "index.jsp" );
                rs.include( request, response );
            }

        } catch (Exception se) {
            se.printStackTrace();
            response.setStatus( 500 );
            out.println( se.getMessage() );
        }
    }
}

