package com.example;

import javax.servlet.RequestDispatcher;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TweetRepository {

    private Connection con;
//    private List<TweetModel> title;
//    private List<TweetModel> author;
//    private List<TweetModel> createdDate;
//    private List<TweetModel> description;
//    private List<TweetModel> email;
//    private List<TweetModel> password;

    private static String SELECT_TO_LOGIN = "select * from student where email=? and pass=?";
    private static String SELECT_TO_LIST_ASC = "select * from tweet order by createdDate asc"; /// ????
    private static String INSERT_TWEET = "insert into tweet (title,description,author) values (?,?,?)";
    private static String SELECT_IF_EXIST_TWEET = "select * from tweet where title=? and description=?";
    private static String INSERT_ACCOUNT = "insert into Student values(?,?,?)";
    private static String SELECT_IF_EXIST_ACCOUNT = "select * from student where name=? and email=?";
    private static String DELETE_TWEET = "delete from tweet where title=? and description=?";
    private static String SELECT_TO_LIST_BY_AUTHOR = "select * from tweet where author=? order by createdDate asc";
    private static String SELECT_TO_VERIFICATION_AUTHOR = "select * from tweet where author=?";


    public TweetRepository() {
    }

    public ResultSet LoginQuery(String  email, String password) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement( SELECT_TO_LOGIN );

        ps.setString( 1, email );
        ps.setString( 2,password );

        ResultSet resultSet = ps.executeQuery();
        return resultSet;
    }

    public int TweetAdded(String title, String description, String author) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement( INSERT_TWEET );

        ps.setString( 1, title );
        ps.setString( 2, description );
        ps.setString( 3,author );

        int result = ps.executeUpdate();
        return result;
    }

    public ResultSet TweetIfExist (String title, String description) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement(SELECT_IF_EXIST_TWEET);
        ps.setString(1, title );
        ps.setString(2, description );

        ResultSet resultSet = ps.executeQuery();
        return resultSet;
    }

    public ResultSet AccountIfExist (String name, String email) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement(SELECT_IF_EXIST_ACCOUNT);
        ps.setString(1, name );
        ps.setString(2, email );

        ResultSet resultSet = ps.executeQuery();
        return resultSet;
    }

    public int AddAccount(String name, String email, String pass) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();

        PreparedStatement ps = con.prepareStatement(INSERT_ACCOUNT);

        ps.setString(1, name);
        ps.setString(2, email);
        ps.setString(3, pass);
        int result = ps.executeUpdate();

        return result;
    }

    public int DeleteTweet (String title, String description) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement( DELETE_TWEET );
        ps.setString( 1, title );
        ps.setString( 2, description );
        int result = ps.executeUpdate();
        return result;
    }


    public ResultSet ListTweetByAuthor (String author) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement( SELECT_TO_LIST_BY_AUTHOR );
        ps.setString( 1, author );
        ResultSet rs = ps.executeQuery();
        return rs;
    }

    public ResultSet AuthorIfExist (String author) throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement( SELECT_TO_VERIFICATION_AUTHOR );
        ps.setString( 1, author );
        ResultSet rs = ps.executeQuery();
        return rs;
    }


    public List<TweetModel> getAllTweets() throws SQLException {
        Connection con = ConnexionDB.ConnexionBase();
        PreparedStatement ps = con.prepareStatement(SELECT_TO_LIST_ASC);
        ResultSet rs = ps.executeQuery();

        return new ArrayList<>();
    }
}
