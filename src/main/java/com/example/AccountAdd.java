package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AccountAdd extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String name = request.getParameter( "name" );
        String email = request.getParameter( "email" );
        String password = request.getParameter( "pass" );

        try {
            int result = new TweetRepository().AddAccount(  name,  email,  password);
            if (result > 0) {
                out.println( "Welcome ! We created your account. " +
                        "Next time please login with your email et password " );
                out.println();
                RequestDispatcher rs = request.getRequestDispatcher( "index2.jsp" );
                rs.forward( request, response );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
