package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Tweet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String title = request.getParameter( "title" );
        String description = request.getParameter( "description" );

        if (Validate.checkExistentTweet( title, description)) {
            RequestDispatcher rs = request.getRequestDispatcher( "TweetAdded" );
            rs.forward( request, response );
        } else {
            out.println( "We can't accept your tweet. The tweet title : \"" + title + " \"" +
                    "and description: \"" + description + "\" already exist.");
            RequestDispatcher rs = request.getRequestDispatcher( "index2.jsp" );
            rs.include( request, response );
        }
    }

}

