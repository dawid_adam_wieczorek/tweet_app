package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DeleteRedirection extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String description = request.getParameter( "description" );
        String title = request.getParameter( "title" );

        if (Validate.checkExistentTweet( title  , description)) {
            RequestDispatcher rs = request.getRequestDispatcher( "DeleteTweet" );
            rs.forward( request, response );
        } else {
            out.println( "We don't have tweet \"" + title + "\" and description: \"" +
                            description + "\" in our database. Please check again." );
            RequestDispatcher rs = request.getRequestDispatcher( "index2.jsp" );
            rs.include( request, response );
        }
    }
}

