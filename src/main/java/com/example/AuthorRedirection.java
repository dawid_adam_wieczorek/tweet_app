package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class AuthorRedirection extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String autho = request.getParameter( "author" );

        if (Validate.checkAuthor(  autho )) {
            RequestDispatcher rs = request.getRequestDispatcher( "ListTweetByAuthor" );
            rs.forward( request, response );
        } else {
            out.println( autho + " doesn't exist in our database. Please check the name." );
            RequestDispatcher rs = request.getRequestDispatcher( "index2.jsp" );
            rs.include( request, response );
        }
    }
}

