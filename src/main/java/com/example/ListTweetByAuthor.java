package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class ListTweetByAuthor extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.getWriter().println( "Blabla" );
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String tit = request.getParameter( "title" );
        String desc = request.getParameter( "description" );
        String auth = request.getParameter( "author" );
        String date = request.getParameter( "createdDate" );

        try {
            ResultSet rs = new TweetRepository().ListTweetByAuthor( auth );
            out.println( "Author: " + auth + " list of his/her tweet" + "<br>");

            while (rs.next()) {
                tit = rs.getString( "title" );
                desc = rs.getString( "description" );
                date = rs.getString( "createdDate" );

                out.print( " created: " + date );
                out.print( " title: " + tit );
                out.print( " description: " + desc + "<br>");

            }

        } catch (Exception se) {
            se.printStackTrace();
            response.setStatus( 500 );
            System.out.println( se.getMessage() );
        }
    }
}

