package com.example;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LoginRedirection extends HttpServlet {

    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String email = request.getParameter( "email" );
        String pass = request.getParameter( "pass" );

        if (Validate.checkAccount( email, pass)) {
            RequestDispatcher rs = request.getRequestDispatcher( "index2.jsp" );
            rs.forward( request, response );
        } else {
            out.println( "Please register in our system" );
            RequestDispatcher rs = request.getRequestDispatcher( "index.jsp" );
            rs.include( request, response );
        }
    }
}



