package com.example;

import java.sql.*;

public class Validate {

     public static boolean checkAuthor(String authorek) {
        boolean st = false;
        try {

            ResultSet rs = new TweetRepository().AuthorIfExist( authorek );
            st = rs.next();


        } catch (Exception e) {
            e.printStackTrace();
        }
        return st;
    }

    public static boolean checkAccount(String email, String password) {
        boolean st = false;
        try {

            ResultSet rs = new TweetRepository().LoginQuery(email, password);
            st = rs.next();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return st;
    }

    public static boolean checkExistentAccount(String name, String email){
        boolean st = false;
        try {

            ResultSet rs = new TweetRepository().AccountIfExist(name, email);
            st = rs.next();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return st;
    }

    public static boolean checkExistentTweet(String title, String description) {
        boolean st = false;
        try{
            ResultSet rs = new TweetRepository().TweetIfExist( title, description );
            st = rs.next();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return st;

    }
}
