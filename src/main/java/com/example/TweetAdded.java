package com.example;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class TweetAdded extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType( "text/html;charset=UTF-8" );
        PrintWriter out = response.getWriter();

        String tite = request.getParameter( "title" );
        String descr = request.getParameter( "description" );
        String auth = request.getParameter( "author" );

        try {
            int i = new TweetRepository().TweetAdded( tite,descr,auth );
            if (i > 0) {
                out.println( "Tweet added" );
                out.println();
                RequestDispatcher rs = request.getRequestDispatcher( "index3.jsp" );
                rs.forward( request, response );
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}


